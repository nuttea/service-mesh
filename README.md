# Service Mesh Workshop

Table of Content

 * [Introduction](#introduction)
 * [Service Mesh Installation](labs/00-install-service-mesh.md)
 * [Microservices Deployment](labs/01-microservice-deployment.md)
 * [Observability with Kiali and Jaeger](labs/02-observability.md)
 * [Dynamic Routing](labs/03-dynamic-routing.md)


## Introduction

WIP



## Authors

* **Voravit** 

